package coinpurse;  

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  
 *  @author Sakara Somapa 5710546411
 */
public class Purse {
    /** Collection of coins in the purse. */
	private ArrayList<Coin> coin;
    
    /** Capacity is maximum NUMBER of coins the purse can hold.
     *  Capacity is set when the purse is created.
     */
    private int capacity;
    
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of coins you can put in purse.
     */
    public Purse( int capacity ) {
    	coin = new ArrayList<Coin>();
    	this.capacity = capacity;
    }

    /**
     * Count and return the number of coins in the purse.
     * This is the number of coins, not their value.
     * @return the number of coins in the purse
     */
    public int count() { return coin.size(); }
    
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public double getBalance() {
    	double sum=0;
    	if(coin.size()!=0) for(int i=0;i<coin.size();i++){
    		sum+=coin.get(i).getValue();
    	}return sum;
    }

    
    /**
     * Return the capacity of the coin purse.
     * @return the capacity
     */
    public int getCapacity() { return capacity; }
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {
    	if(getCapacity()<=count()) return true;
        return false;
    }

    /** 
     * Insert a coin into the purse.
     * The coin is only inserted if the purse has space for it
     * and the coin has positive value.  No worthless coins!
     * @param coin is a Coin object to insert into purse
     * @return true if coin inserted, false if can't insert
     */
    public boolean insert( Coin coin ) {
    	if(!(isFull())){
    		if(coin.getValue()>0){
	    		this.coin.add(coin);
	    		Collections.sort(this.coin);
	    		return true;
    		}
    	}return false;
    }
    
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of Coins withdrawn from purse,
     *  or return null if the amount requested aren't appropriate.
     *  @param amount is the amount to withdraw
     *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    public Coin[] withdraw( double amount ) {
        if(amount <= 0) return null;
        ArrayList<Coin> temp = new ArrayList<Coin>();
        Collections.sort(this.coin);
        for(int i=coin.size()-1;i>=0;i--){
        	if(amount>=coin.get(i).getValue()){
        		amount-=coin.get(i).getValue();
        		temp.add(coin.get(i));
        	}
        }if ( amount > 0 ){
			return null;
		}Collections.sort(temp);
        Coin[] arrCoin = new Coin[temp.size()];
        for(int i=0;i<temp.size();i++){
        	arrCoin[i] = temp.get(i);
        }for(int i=0;i<coin.size();i++){
        	for(int j=0;j<temp.size();j++){
            	if(temp.get(j).equals(coin.get(i))){
            		temp.remove(j);
            		coin.remove(i);
            		i=0;j=-1;
            	}
            }
        }
        return arrCoin;
	}
  
    /** 
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     */
    public String toString() {
        return String.format("%d coins with value %.1f",coin.size(),getBalance());
    }

}
