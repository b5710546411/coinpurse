package coinpurse; 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Sakara Somapa 5710546411
 */
public class Coin implements Comparable<Coin> {

    /** Value of the coin */
    private final double value;
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ) {
        this.value = value;
    }

    /** Return the value of the coin. */
    public double getValue(){
    	return value;
    }
    
    /** Check weather object is the same. */
    public boolean equals(Object obj){
    	if(obj == null) return false;
    	if(obj.getClass() != this.getClass()) return false;
    	if (value == ((Coin)obj).value) return true;
    	return false;
    }
    
    /** Compare weather object is equal, more or less. */
    public int compareTo(Coin o) {
    	if (value>o.getValue()) return 1;
    	if (value==o.getValue()) return 0;
    	return -1;
    }
    
    /** Compare weather object is equal, more or less. */
    public String toString(){
    	return String.format("%.2f",value);
    }
    
}
